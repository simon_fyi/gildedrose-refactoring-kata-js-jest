module.exports = {
  comments: false,
  presets: [
    [
      '@babel/preset-env',
    ],
  ],
  ignore: ['node_modules'],
};
