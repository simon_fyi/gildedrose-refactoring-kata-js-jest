/* dependencies */
import { productTypes } from './products/types';

/*
 *  Item class
 *  Do not alter, owned by "the goblin in the corner"
 */
export class Item {
  constructor(name, sellIn, quality){
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

/*
 *  Shop class
 */
export class Shop {
  constructor(items = []) {
    this.items = items;
  }

  updateQuality() {
    return this.items.map(product => {
      const productType = (
        productTypes.find(type => type.name === product.name)
        || productTypes.find(type => type.name === 'default')
      );

      return productType.mutator && typeof productType.mutator === 'function'
        ? productType.mutator(product)
        : product;
    });
  }
}
