/* dependencies */
import {
  decreaseSellIn,
  increaseQuality,
  minimizeQuality,
} from '../mutators';

/* mutator */
export const productTypeMutator = product => {
  increaseQuality(product);

  if (product.sellIn < 11) {
    increaseQuality(product);
  }

  if (product.sellIn < 6) {
    increaseQuality(product);
  }

  decreaseSellIn(product);

  if (product.sellIn < 0) {
    minimizeQuality(product);
  }

  return product;
};
