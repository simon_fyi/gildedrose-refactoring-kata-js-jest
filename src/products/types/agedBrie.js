/* dependencies */
import {
  decreaseSellIn,
  increaseQuality,
} from '../mutators';

/* mutator */
export const productTypeMutator = product => {
  increaseQuality(product);

  decreaseSellIn(product);

  if (product.sellIn < 0) {
    increaseQuality(product);
  }

  return product;
};
