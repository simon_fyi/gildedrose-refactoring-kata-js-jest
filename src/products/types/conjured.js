/* dependencies */
import {
  decreaseQuality,
  decreaseSellIn
} from '../mutators';

/* mutator */
export const productTypeMutator = product => {
  decreaseQuality(product);
  decreaseQuality(product);

  decreaseSellIn(product);

  if (product.sellIn < 0) {
    decreaseQuality(product);
    decreaseQuality(product);
  }

  return product;
};
