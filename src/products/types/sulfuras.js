/*
 *  mutator
 *
 *  note: Default 'quality' is 80 and it never alters
 */
export const productTypeMutator = product => {
  product.quality = 80;

  return product;
};
