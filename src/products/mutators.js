/* constants */
const productQualityMin = 0;
const productQualityMax = 50;

/* mutators */
export const decreaseSellIn = product => {
  product.sellIn -= 1;
};

export const decreaseQuality = product => {
  product.quality = Math.max(product.quality - 1, productQualityMin);
};

export const increaseQuality = product => {
  product.quality = Math.min(product.quality + 1, productQualityMax);
};

export const minimizeQuality = product => {
  product.quality = productQualityMin;
};
