/* consts */
import { productTypeMutator as agedBrieMutator }  from './types/agedBrie';
import { productTypeMutator as backstagePassesMutator } from './types/backstagePasses';
import { productTypeMutator as conjuredMutator } from './types/conjured';
import { productTypeMutator as defaultMutator } from './types/default';
import { productTypeMutator as sulfurasMutator } from './types/sulfuras';

export const productTypes = [
  {
    name: 'default',
    mutator: defaultMutator,
  },
  {
    name: 'conjured',
    mutator: conjuredMutator,
  },
  {
    name: 'Aged Brie',
    mutator: agedBrieMutator,
  },
  {
    name: 'Backstage passes to a TAFKAL80ETC concert',
    mutator: backstagePassesMutator,
  },
  {
    name: 'Sulfuras, Hand of Ragnaros',
    mutator: sulfurasMutator,
  }
];
