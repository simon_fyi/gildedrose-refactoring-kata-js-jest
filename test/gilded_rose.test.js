/* dependencies */
const { Shop, Item } = require("../src/gilded_rose");

/* helpers */
const runEmptyUpdateQuality = function () {
  const gildedRose = new Shop();
  const items = gildedRose.updateQuality();
  return items;
};

const runUpdateQuality = function (name, sellIn, quality) {
  const gildedRose = new Shop([new Item(name, sellIn, quality)]);
  const items = gildedRose.updateQuality();
  return items;
};

/* testers */
describe("Gilded Rose", function() {
  /* original test rewritten */
  it("should foo", function() {
    const items = runUpdateQuality("foo", 0, 0);
    expect(items[0].name).toBe("foo");
  });

  /* test empty[] of Shop constructor */
  it("should run updateQuality even with no products", function() {
    expect(runEmptyUpdateQuality()).toMatchSnapshot();
  });

  /* test various 'foo' products */
  it("should run updateQuality for various 'foo'", function() {
    const productName = 'foo';
    expect(runUpdateQuality(productName, 0, 0)).toMatchSnapshot();
    expect(runUpdateQuality(productName, 0, 5)).toMatchSnapshot();
  });

  /* test various 'conjured' products */
  it("should run updateQuality for various 'conjured'", function() {
    const productName = 'conjured';
    expect(runUpdateQuality(productName, 0, 0)).toMatchSnapshot();
    expect(runUpdateQuality(productName, 0, 5)).toMatchSnapshot();
  });

  /* test various 'Aged brie' products */
  it("should run updateQuality for various 'Aged Brie'", function() {
    const productName = 'Aged Brie';
    expect(runUpdateQuality(productName, 0, 0)).toMatchSnapshot();
    expect(runUpdateQuality(productName, 0, 75)).toMatchSnapshot();
  });

  /* test various 'Backstage passes' products */
  it("should run updateQuality for various 'Backstage passes'", function() {
    const productName = 'Backstage passes to a TAFKAL80ETC concert';
    expect(runUpdateQuality(productName, 0, 0)).toMatchSnapshot();
    expect(runUpdateQuality(productName, 0, 49)).toMatchSnapshot();
    expect(runUpdateQuality(productName, 0, 75)).toMatchSnapshot();
    expect(runUpdateQuality(productName, 1, 0)).toMatchSnapshot();
    expect(runUpdateQuality(productName, 15, 0)).toMatchSnapshot();
    expect(runUpdateQuality(productName, 15, 75)).toMatchSnapshot();
  });

  /* test various 'Sulfuras' products */
  it("should run updateQuality for various 'Sulfuras'", function() {
    const productName = 'Sulfuras, Hand of Ragnaros';
    expect(runUpdateQuality(productName, -1, 5)).toMatchSnapshot();
    expect(runUpdateQuality(productName, 0, 0)).toMatchSnapshot();
    expect(runUpdateQuality(productName, 0, 5)).toMatchSnapshot();
  });

});
